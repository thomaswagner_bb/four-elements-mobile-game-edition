﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBoundTriggerManager : MonoBehaviour
{
    private GameObject gameController;          //Reference to main game controller

    void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }

    public void OnTriggerEnter(Collider other)
    {
        switch(other.gameObject.tag)
        {
            case "Player":
                gameController.GetComponent<GlobalGameManager>().DecrementP1PawnCount();
                StartCoroutine(gameController.GetComponent<GlobalGameManager>().managePostKO());
                if (GlobalGameManager.playersTurn)
                {
                    GlobalGameManager.playerGoals--;
                }
                else
                {
                    GlobalGameManager.opponentGoals++;
                }
                break;

            case "Player1_Lord":
                GlobalGameManager.player1NumPawns = 0; // Game finished if lord is out!
                StartCoroutine(gameController.GetComponent<GlobalGameManager>().managePostKO());
                if (GlobalGameManager.playersTurn)
                {
                    GlobalGameManager.playerGoals -= GlobalGameManager.player2NumPawns;
                }
                else
                {
                    GlobalGameManager.opponentGoals += GlobalGameManager.player1NumPawns;
                }
                break;

            case "Player_2":
                gameController.GetComponent<GlobalGameManager>().DecrementP2PawnCount();
                StartCoroutine(gameController.GetComponent<GlobalGameManager>().managePostKO());
                if (GlobalGameManager.playersTurn)
                {
                    GlobalGameManager.playerGoals++;
                }
                else
                {
                    GlobalGameManager.opponentGoals--;
                }
                break;

            case "Player2_Lord":
                GlobalGameManager.player2NumPawns = 0; // Game finished if lord is out!
                StartCoroutine(gameController.GetComponent<GlobalGameManager>().managePostKO());
                if (GlobalGameManager.playersTurn)
                {
                    GlobalGameManager.playerGoals += GlobalGameManager.player2NumPawns;
                }
                else
                {
                    GlobalGameManager.opponentGoals -= GlobalGameManager.player1NumPawns;
                }
                break;
        }

        
        // Destroy the object once out of bounds
        Destroy(other.gameObject);
    }
}
