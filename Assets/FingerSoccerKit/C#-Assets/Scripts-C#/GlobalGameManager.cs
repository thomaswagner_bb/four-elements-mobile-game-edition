using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

public class GlobalGameManager : MonoBehaviour {
		
	///*************************************************************************///
	/// Main Game Controller.
	/// This class controls main aspects of the game like rounds, levels, scores and ...
	/// Please note that the game always happens between 2 player: (Player-1 vs Player-2) or (Player-1 vs AI)
	/// Player-2 and AI are the same in some aspects like when they got their turns, but they use different controllers.
	/// Player-2 uses a similar controller as Player-1, while AI uses an artificial intelligent routine to play the game.
	///
	/// Important! All units and ball object inside the game should be fixed at Z=-0.5f positon at all times. 
	/// You can do this with RigidBody's freeze position.
	///*************************************************************************///

	public static string player1Name = "Player_1";
	public static string player2Name = "Player_2";
	public static string cpuName = "CPU";

	// Available Game Modes:
	/*
	Indexes:
	0 = 1 player against cpu (normal or Tournament)
	1 = 2 player against each other on the same platform/device
	2 = Penalty Kicks
	*/
	public static int gameMode;

	//Odd rounds are player (Player-1) turn and Even rounds are AI (Player-2)'s
	public static int round;

	//in-game formation buttons (these are not needed in penalty kicks mode!)
	public GameObject p1FormationButton;
	public GameObject p2FormationButton;


	//available time to think and shoot
	public static float baseShootTime = 15;		//fixed shoot time for all players and AI
	private float p1ShootTime;					//additional time (based on the selected team) for p1
	private float p2ShootTime;					//additional time (based on the selected team) for p2 or AI
	public GameObject p1TimeBar;
	public GameObject p2TimeBar;
	private float p1TimeBarInitScale;
	private float p1TimeBarCurrentScale;
	private float p2TimeBarInitScale;
	private float p2TimeBarCurrentScale;

	//Maximum distance that players can drag away from selected unit to shoot the ball (is in direct relation with shoot power)
	public static float maxDistance = 5.0f;

	//Turns in flags
	public static bool playersTurn;
	public static bool opponentsTurn;

	//After players did their shoots, the round changes after this amount of time.
	public static float timeStepToAdvanceRound = 0.5f; 

	//Special occasions
	public static bool goalHappened;
	public static bool shootHappened;
	public static bool gameIsFinished;
	public static int goalLimit = 10; //To finish the game quickly, without letting the GameTime end.

	///Game timer vars
	public static float gameTimer; //in seconds
	private string remainingTime;
	private int seconds;
	private int minutes;

	//Game Status
	public static int playerGoals;
	public static int opponentGoals;
	public static float gameTime; //Main game timer (in seconds). Always fixed.

    public static int player1NumPawns, player2NumPawns;

	//summary information
	public static int playerPasses;
	public static int playerShoots;
	public static int playerShootToGate;
	public static int opponentPasses;
	public static int opponentShoots;
	public static int opponentShootToGate;

	//gameObject references
	private GameObject playerAIController;
	private GameObject opponentAIController;
	public GameObject goalPlane;

    // Teams objects
    public static GameObject[] playerTeam;      //array of all player-1 units
    public static int playerFormation;          //player-1 formation
    public static int playerFlag;               //player-1 team flag

    public static GameObject[] player2Team;     //array of all player-2 units
    public static int player2Formation;         //player-2 formation
    public static int player2Flag;              //player-2 team flag

    //Public references
    public GameObject gameStatusPlane;			//user to show win/lose result at the end of match
	public GameObject continueTournamentBtn;	//special tournament button to advance in tournament incase of win
	public GameObject statusTextureObject;		//plane we use to show the result texture in 3d world
	public Texture2D[] statusModes;				//Available status textures

    private GameObject[] P1Team;
    private GameObject[] P2Team;

    //*****************************************************************************
    // Init. 
    //*****************************************************************************
    void Awake (){	

		//debug
		//PlayerPrefs.DeleteAll();

		//init
		goalHappened = false;
		shootHappened = false;
		gameIsFinished = false;
		playerGoals = 0;
		opponentGoals = 0;
		gameTime = 0;
		round = 1;
		seconds = 0;
		minutes = 0;

		//reset summary info
		playerPasses = 0;
		playerShoots = 0;
		playerShootToGate = 0;
		opponentPasses = 0;
		opponentShoots = 0;
		opponentShootToGate = 0;

		//get additonal time for each player and AI
		p1ShootTime = baseShootTime + TeamsManager.getTeamSettings(PlayerPrefs.GetInt("PlayerFlag")).y;
		p2ShootTime = baseShootTime + TeamsManager.getTeamSettings(PlayerPrefs.GetInt("Player2Flag")).y;
		print ("P1 shoot time: " + p1ShootTime + " // " + "P2 shoot time: " + p2ShootTime);
		
		//hide gameStatusPlane
		gameStatusPlane.SetActive(false);
		continueTournamentBtn.SetActive(false);
		
		//Translate gameTimer index to actual seconds
		switch(PlayerPrefs.GetInt("GameTime")) {
			case 0:
				gameTimer = 180;
				break;
			case 1:
				gameTimer = 300;
				break;
			case 2:
				gameTimer = 480;
				break;
			
			//You can add more cases and options here.
		}

		p1TimeBarInitScale = p1TimeBar.transform.localScale.x;
		p1TimeBarCurrentScale = p1TimeBar.transform.localScale.x;
		p2TimeBarInitScale = p2TimeBar.transform.localScale.x;
		p2TimeBarCurrentScale = p2TimeBar.transform.localScale.x;
		p1TimeBar.transform.localScale = new Vector3(1,1,1);
		p2TimeBar.transform.localScale = new Vector3(1,1,1);
		
		//Get Game Mode
		if(PlayerPrefs.HasKey("GameMode"))
			gameMode = PlayerPrefs.GetInt("GameMode");
		else
			gameMode = 0; // Default Mode (Player-1 vs AI)
		
		playerAIController = GameObject.FindGameObjectWithTag("playerAI");
		opponentAIController = GameObject.FindGameObjectWithTag("opponentAI");

        //ref GameObject[] Pl1Team = ref PlayerAIV2.playerTeam;
        //ref GameObject[] Pl2Team = ref PlayerAIV2.player2Team;


        //// Link references to teams objects
        //playerTeam = PlayerAIV2.playerTeam;
        //player2Team = PlayerAIV2.player2Team;

        manageGameModes();
	}
		

	//*****************************************************************************
	// We have all units inside the game scene by default, but at the start of the game,
	// we check which side in playing (should be active) and deactive the side that is
	// not playing by deactivating all it's units.
	//*****************************************************************************
	//private GameObject[] P2Team;	    //array of all player-2 units in the game
	private GameObject[] cpuTeam;		//array of all AI units in the game
	void manageGameModes ()
    {
		switch(gameMode) {
		case 0:
			//find and deactive all player2 units. This is player-1 vs AI.
			P2Team = GameObject.FindGameObjectsWithTag("Player_2");
			foreach (GameObject unit in P2Team) {
				unit.SetActive (false);
			}

			p2FormationButton.SetActive (false);

			break;
		
		case 1:
			//find and deactive all AI Opponent units. This is Player-1 vs Player-2.
			cpuTeam = GameObject.FindGameObjectsWithTag("Opponent");
			foreach(GameObject unit in cpuTeam) {
				unit.SetActive(false);
			}
			//deactive opponent's AI
			opponentAIController.SetActive(false);
			break;
		}
	}

	IEnumerator Start (){
		roundTurnManager();
		yield return new WaitForSeconds(1.5f);
	}

	//*****************************************************************************
	// FSM
	//*****************************************************************************
	void Update (){
		//check game finish status every frame
		if(!gameIsFinished) {

			manageGameStatus();

			//fill time limit bars, only in normal game mode
			updateTimeBars();
		}
    }

	//*****************************************************************************
	// This function gives turn to players in the game.
	//*****************************************************************************
	public static string whosTurn;
	void roundTurnManager ()
    {	
		if(gameIsFinished || goalHappened)
			return;

		//reset shootHappened flag
		shootHappened = false;

		//fill time limit bars, only in normal game mode
		fillTimeBar(1);
		fillTimeBar(2);

        //if round number is odd, it's players turn, else it's AI or player-2 's turn
		int carry;
		carry = round % 2;
		if(carry == 1) {
			playersTurn = true;
			opponentsTurn = false;
			playerController.canShoot = true;
            playerControllerV2.canShoot = true;
            OpponentAI.opponentCanShoot = false;

            whosTurn = "player";
		} else {
			playersTurn = false;
			opponentsTurn = true;
			playerController.canShoot = false;
            playerControllerV2.canShoot = false;
            OpponentAI.opponentCanShoot = true;

            whosTurn = "opponent";
		}

        //Override
        //for two player game, players can always shoot.
        //we override this because both human players play on the same device and must be able to shoot at every turn.
        //we just limit their actions to their own units.
        if (gameMode == 1 || gameMode == 2)
        {
            playerController.canShoot = true;
            playerControllerV2.canShoot = true;
        }
    }


	//*****************************************************************************
	// Update timebars by changing their scale (x) over time.
	// If time ends, the turn will be transforred to opponent.
	//*****************************************************************************
	void updateTimeBars() {

		if(gameIsFinished || goalHappened || shootHappened)
			return;

		//limiters and turn change
		//Also change turns incase of time running out!
		if(p1TimeBarCurrentScale <= 0) {
			p1TimeBarCurrentScale = 0;
			setNewRound(1);
			return;
		}
		if(p2TimeBarCurrentScale <= 0) {
			p2TimeBarCurrentScale = 0;
			setNewRound(2);
			return;
		}
		

		if(playersTurn) {
			p1TimeBarCurrentScale -= Time.deltaTime / p1ShootTime;
			p1TimeBar.transform.localScale = new Vector3(p1TimeBarCurrentScale, p1TimeBar.transform.localScale.y, p1TimeBar.transform.localScale.z);
			fillTimeBar(2);
		} else {
			p2TimeBarCurrentScale -= Time.deltaTime / p2ShootTime;
			p2TimeBar.transform.localScale = new Vector3(p2TimeBarCurrentScale, p2TimeBar.transform.localScale.y, p2TimeBar.transform.localScale.z);
			fillTimeBar(1);
		}
	}


	void fillTimeBar(int _ID) {
		if(_ID == 1) {
			p1TimeBarCurrentScale = p1TimeBarInitScale;
			p1TimeBar.transform.localScale = new Vector3(1,1,1);
		} else {
			p2TimeBarCurrentScale = p2TimeBarInitScale;
			p2TimeBar.transform.localScale = new Vector3(1,1,1);
		}
	}

	void setNewRound(int _ID) {
		switch(_ID) {
		case 1:
			round = 2;
			break;		
		case 2:
			round = 1;
			break;	
		}	
		roundTurnManager();
	}

	//*****************************************************************************
	// What happens after a shoot is performed?
	//*****************************************************************************
	public IEnumerator managePostShoot ( string _shootBy  ){

		shootHappened = true;

		//get who is did the shoot
		//if we had a goal after the shoot was done and just before the round change, leave the process to other controllers.
		float t = 0;
		while(t < timeStepToAdvanceRound) {	
			t += Time.deltaTime;
			if(goalHappened) {
				yield break;
			} 		
			yield return 0;
		}
		
		//we had a simple shoot with no goal result
		if(t >= timeStepToAdvanceRound) {
			//add to round counters
			switch(_shootBy) {
				case "Player":
				case "Player1_Lord":
					round = 2;
					break;		
				case "Player_2":
				case "Player2_Lord":
					round = 1;
					break;	
				case "Opponent":
					round = 1;
					break;
			}	
			
			//let the units get to their positions
			yield return new WaitForSeconds(0.5f);

			roundTurnManager(); //cycle again between players
		}
	}
			
	//*****************************************************************************
	// If we had an element pushed out of the field in this round, this is the
    // function that manages all aspects of it.
	//*****************************************************************************								
	public IEnumerator managePostKO (){

        //avoid counting a goal as two or more
        if (goalHappened) {
            yield break;
        }
		
		//soft pause the game for reformation and other things...
		goalHappened = true;
		shootHappened = false;
                     
        // Switch round to next player
        if (playersTurn)
        {
            round = 2;
        }
        else
        {
            round = 1;
        }

		//activate the goal event plane
		GameObject gp = null;
		gp = Instantiate (goalPlane, new Vector3(30, 0, -2), Quaternion.Euler(0, 180, 0)) as GameObject;
		float t = 0;
		float speed = 2.0f;
		while(t < 1) {
			t += Time.deltaTime * speed;
			gp.transform.position = new Vector3(Mathf.SmoothStep(30, 0, t), 0, -2);
			yield return 0;
		}
		yield return new WaitForSeconds(0.5f);
		float t2 = 0;
		while(t2 < 1) {
			t2 += Time.deltaTime * speed;
			gp.transform.position = new Vector3(Mathf.SmoothStep(0, -40, t2), 0, -2);
			yield return 0;
		}
		Destroy(gp, 1.5f);

        if (player1NumPawns > 0 || player2NumPawns > 0)
        {
            Debug.Log("Number of units left - P1=" + player1NumPawns + " P2=" + player2NumPawns);
        }

        P1Team = PlayerAIV2.playerTeam;
        P2Team = PlayerAIV2.player2Team;

        // Check if the game is finished or not.
        // This code needs some fixing to handle elements and lords
        // separately.
        if (player1NumPawns == 0 || player2NumPawns == 0 ||
           (player1NumPawns == 1 && P1Team[0].tag == "Player1_Lord") ||
           (player2NumPawns == 1 && P2Team[0].tag == "Player2_Lord"))
        {
            manageGameFinishState();
            gameIsFinished = true;
        }
		
		//else, continue to the next round
		goalHappened = false;
		roundTurnManager();
	}

	//***************************************************************************//
	// Game status manager
	//***************************************************************************//
	public GameObject timeText;				//UI 3d text object
	public GameObject playerGoalsText;		//UI 3d text object
	public GameObject opponentGoalsText;	//UI 3d text object
	public GameObject playerOneName;		//UI 3d text object
	public GameObject playerTwoName;		//UI 3d text object
	void manageGameStatus (){

		seconds = Mathf.CeilToInt(gameTimer - Time.timeSinceLevelLoad) % 60;
		minutes = Mathf.CeilToInt(gameTimer - Time.timeSinceLevelLoad) / 60; 
		
		if(seconds == 0 && minutes == 0) {
			gameIsFinished = true;
			manageGameFinishState();
		}
		
		remainingTime = string.Format("{0:00} : {1:00}", minutes, seconds); 
		timeText.GetComponent<TextMesh>().text = remainingTime.ToString();

        playerGoalsText.GetComponent<TextMesh>().text = playerGoals.ToString();
        opponentGoalsText.GetComponent<TextMesh>().text = opponentGoals.ToString();

		if(gameMode == 0) {
			playerOneName.GetComponent<TextMesh>().text = player1Name;
			playerTwoName.GetComponent<TextMesh>().text = cpuName;
		} else if(gameMode == 1 || gameMode == 2) {
			playerOneName.GetComponent<TextMesh>().text = player1Name;
			playerTwoName.GetComponent<TextMesh>().text = player2Name;
		} 
	}

    //*****************************************************************************
    // After the game is finished, this function handles the events.
    //*****************************************************************************
    public void manageGameFinishState() {

        print("GAME IS FINISHED.");

        //show gameStatusPlane
        gameStatusPlane.SetActive(true);

        //for single player game, we should give the player some bonuses in case of winning the match
        if (gameMode == 0) {
            if (playerGoals > goalLimit || playerGoals > opponentGoals) {
                print("Player 1 is the winner!!");

                //set the result texture
                statusTextureObject.GetComponent<Renderer>().material.mainTexture = statusModes[0];

                int playerWins = PlayerPrefs.GetInt("PlayerWins");
                int playerMoney = PlayerPrefs.GetInt("PlayerMoney");

                PlayerPrefs.SetInt("PlayerWins", ++playerWins);         //add to wins counter
                PlayerPrefs.SetInt("PlayerMoney", playerMoney + 100);   //handful of coins as the prize!

                //if this is a tournament match, update it with win state and advance.
                if (PlayerPrefs.GetInt("IsTournament") == 1) {
                    PlayerPrefs.SetInt("TorunamentMatchResult", 1);
                    PlayerPrefs.SetInt("TorunamentLevel", PlayerPrefs.GetInt("TorunamentLevel", 0) + 1);
                    continueTournamentBtn.SetActive(true);
                }

            } else if (opponentGoals > goalLimit || opponentGoals > playerGoals) {

                print("CPU is the winner!!");
                statusTextureObject.GetComponent<Renderer>().material.mainTexture = statusModes[1];

                //if this is a tournament match, update it with lose state.
                if (PlayerPrefs.GetInt("IsTournament") == 1) {
                    PlayerPrefs.SetInt("TorunamentMatchResult", 0);
                    PlayerPrefs.SetInt("TorunamentLevel", PlayerPrefs.GetInt("TorunamentLevel", 0) + 1);
                    continueTournamentBtn.SetActive(true);
                }

            } else if (opponentGoals == playerGoals) {

                print("(Single Player) We have a Draw!");
                statusTextureObject.GetComponent<Renderer>().material.mainTexture = statusModes[4];

                //we count "draw" a lose in tournament mode.
                //if this is a tournament match, update it with lose state.
                if (PlayerPrefs.GetInt("IsTournament") == 1) {
                    PlayerPrefs.SetInt("TorunamentMatchResult", 0);
                    PlayerPrefs.SetInt("TorunamentLevel", PlayerPrefs.GetInt("TorunamentLevel", 0) + 1);
                    continueTournamentBtn.SetActive(true);
                }
            }
        } else if (gameMode == 1) {
            if (player2NumPawns == 0) {
                print("Player 1 is the winner!!");
            statusTextureObject.GetComponent<Renderer>().material.mainTexture = statusModes[2];
            } else if (player2NumPawns == 0 && player1NumPawns == 0) {
            print("(Two-Player) We have a Draw!");
            statusTextureObject.GetComponent<Renderer>().material.mainTexture = statusModes[4];
            } else if (player1NumPawns == 0) {
				print("Player 2 is the winner!!");
				statusTextureObject.GetComponent<Renderer>().material.mainTexture = statusModes[3];
			} 
		}

        // Clear pawns count
        ClearPawnsCount();

    }


	//*****************************************************************************
	// Play sound clips
	//*****************************************************************************
	void playSfx ( AudioClip _clip  ){
		GetComponent<AudioSource>().clip = _clip;
		if(!GetComponent<AudioSource>().isPlaying) {
			GetComponent<AudioSource>().Play();
		}
	}

    private void ClearPawnsCount()
    {
        player1NumPawns = 0;
        player2NumPawns = 0;
    }

    public void IncrementP1PawnCount()
    {
        player1NumPawns++;
    }

    public void IncrementP2PawnCount()
    {
        player2NumPawns++;
    }

    public void DecrementP1PawnCount()
    {
        player1NumPawns--;
    }

    public void DecrementP2PawnCount()
    {
        player2NumPawns--;
    }

    public void DisplayPawnsNumber()
    {
        Debug.Log("P1 = " + player1NumPawns);
        Debug.Log("P2 = " + player2NumPawns);
    }

    public string WhichPlayersTurn()
    {
        if (playersTurn)
            return "Player";

        return "Opponent";
    }

}
