#pragma warning disable 0219

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class playerControllerV2 : MonoBehaviour {

	///*************************************************************************///
	/// Main Player Controller.
	/// This class manages the shooting process of human players.
	/// This also handles the rendering of debug lines in editor.
	///*************************************************************************///

	// Public Variables
	internal int unitIndex;				//This unit's ID (given automatically by PlayerAI class)
	public GameObject selectionCircle;	//Reference to gameObject

	//Referenced GameObjects
	private GameObject helperBegin; 	//Start helper
	private GameObject helperEnd; 		//End Helper
	private GameObject arrowPlane; 		//arrow plane which is used to show shotPower
	private GameObject shootCircle;		//shoot Circle plane which is used to show shotPower
    private const float shootCircleScale = 1.2f;

	//For Update 1.7.x
	private GameObject aimPivot;		//starting point for aim arrow
	private GameObject aimArrow;		//arrow plane which is used to show aim precision (the holder)
	private GameObject aimArrowBody;	//arrow plane which is used to show aim precision

	private GameObject gameController;	//Reference to main game controller
	private float currentDistance;		//real distance of our touch/mouse position from initial drag position
	private float safeDistance; 			//A safe distance value which is always between min and max to avoid supershoots

    private GameObject selectedElementCircleObject;
    private MeshRenderer selectedElementRenderer;
    private float dragTime;             //drag time
    private float dragTimeUnit;         //Normalised drag time
	private float pwr;					//shoot power

	//this vector holds shooting direction
	private Vector3 shootDirectionVector;

    // When the controlled element collides with another, its course is locked.
    private bool lockElementToFlick;
    private Vector3 elementLockPosition;

    //prevent player to shoot twice in a round
    public static bool canShoot;
	internal float shootTime;
	private int timeAllowedToShoot = 10000; //In Seconds (in this kit we give players unlimited time to perform their turn of shooting)

    // Jitter for shooting arrow
    private float arrowJitterRatio, arrowJitterAngle;

    // Constant values
    private const float maxDragTime = 1f;
    private const float initSelectCircleScale = 1f;
    private const float maxSelectCircleScale = 3f;

    //*****************************************************************************
    // Init
    //*****************************************************************************
    void Awake (){
		//Find and cache important gameObjects
		helperBegin = GameObject.FindGameObjectWithTag("mouseHelperBegin");
		helperEnd = GameObject.FindGameObjectWithTag("mouseHelperEnd");
		arrowPlane = GameObject.FindGameObjectWithTag("helperArrow");
		gameController = GameObject.FindGameObjectWithTag("GameController");
		shootCircle = GameObject.FindGameObjectWithTag("shootCircle");

		//V1.7.x
		aimArrow = GameObject.FindGameObjectWithTag("aimArrow");	
		aimArrowBody = GameObject.FindGameObjectWithTag("aimArrowBody");
		aimPivot = GameObject.FindGameObjectWithTag("aimPivot");

        //Init Variables
        dragTime = 0f;
        pwr = 0.1f;
		currentDistance = 0;
		shootDirectionVector = new Vector3(0,0,0);
		canShoot = true;
		shootTime = timeAllowedToShoot;
		arrowPlane.GetComponent<Renderer>().enabled = false;	 //hide arrowPlane
		shootCircle.GetComponent<Renderer>().enabled = false;	 //hide shoot Circle
		aimArrowBody.GetComponent<Renderer>().enabled = false;	 //hide aim arrow

        lockElementToFlick = false;

    }

	void Start (){
		// Add count to main game pawn counter
        switch (gameObject.tag)
        {
            case "Player":
            case "Player1_Lord":
                gameController.GetComponent<GlobalGameManager>().IncrementP1PawnCount();
                break;
            case "Player_2":
            case "Player2_Lord":
                gameController.GetComponent<GlobalGameManager>().IncrementP2PawnCount();
                break;
            default:
                Debug.Log("No valid object");
                break;
        }        
	}

	void Update (){
		
		//Active the selection circles around Player units when they have the turn.
		if((GlobalGameManager.playersTurn && gameObject.tag == "Player" ||
            GlobalGameManager.playersTurn && gameObject.tag == "Player1_Lord") && !GlobalGameManager.goalHappened)
			selectionCircle.GetComponent<Renderer>().enabled = true;
		else if((GlobalGameManager.opponentsTurn && gameObject.tag == "Player_2" ||
                 GlobalGameManager.opponentsTurn && gameObject.tag == "Player2_Lord") && !GlobalGameManager.goalHappened)
			selectionCircle.GetComponent<Renderer>().enabled = true;			
		else	
			selectionCircle.GetComponent<Renderer>().enabled = false;	
	}

    // true=enable, false=disable
    private void SetProximityDetection(bool state)
    {
        // De/Activate player 1 units proximity detection
        foreach (GameObject unit in PlayerAIV2.playerTeam)
        {
            if (unit != null)
                unit.GetComponent<CapsuleCollider>().enabled = state;
        }

        // De/Activate player 2 units proximity detection
        foreach (GameObject unit in PlayerAIV2.player2Team)
        {
            if (unit != null)
                unit.GetComponent<CapsuleCollider>().enabled = state;
        }
    }

    //***************************************************************************//
    // Works fine with mouse and touch
    // This is the main function used to manage drag on units, calculating the
    // power and debug vectors, and set the final parameters to shoot.
    //***************************************************************************//
    static float maxpwr = Mathf.Abs(GlobalGameManager.maxDistance) * Mathf.Abs(GlobalGameManager.maxDistance) * 4;

    void OnMouseDrag (){
		if( canShoot && ((GlobalGameManager.playersTurn && gameObject.tag == "Player") ||
                         (GlobalGameManager.playersTurn && gameObject.tag == "Player1_Lord") ||
                         (GlobalGameManager.opponentsTurn && gameObject.tag == "Player_2") ||
                         (GlobalGameManager.opponentsTurn && gameObject.tag == "Player2_Lord")) )
			{
                // The longer the drag time the higher the flicking power
                dragTime = (dragTime < maxDragTime) ? dragTime + Time.deltaTime : dragTime;
                dragTimeUnit = dragTime / maxDragTime;
                currentDistance = Vector3.Distance(helperBegin.transform.position, shootCircle.transform.position);
				
				//limiters
				if(currentDistance <= GlobalGameManager.maxDistance)
					safeDistance = currentDistance;
				else
					safeDistance = GlobalGameManager.maxDistance;

                // Power is increased the longer the dragging
                if (gameObject.tag == "Player1_Lord" || gameObject.tag == "Player2_Lord")
                    pwr = gameObject.GetComponent<Rigidbody>().mass * Mathf.Pow(dragTime, 2) * 60f;
                else
                    pwr = gameObject.GetComponent<Rigidbody>().mass * Mathf.Pow(dragTime, 2) * 120f;

                // As power is increased the colour of the size of the selection circle grows and tends towards its "Full power" colour.
                selectedElementCircleObject.GetComponent<ScaleAnimator>().UpdateAnimationScale(Mathf.SmoothStep(initSelectCircleScale, maxSelectCircleScale, dragTimeUnit));
                if (gameObject.tag == "Player")
                {
                    selectedElementRenderer.material.color = Color.Lerp(Color.white, TeamsManager.getTeamShade((PlayerPrefs.GetInt("PlayerFlag"))), dragTimeUnit);
                }
                else if (gameObject.tag == "Player_2")
                {
                    selectedElementRenderer.material.color = Color.Lerp(Color.white, TeamsManager.getTeamShade((PlayerPrefs.GetInt("Player2Flag"))), dragTimeUnit);
                }

                //show the power arrow above the unit and scale is accordingly.
                manageArrowTransform();
				
				//position of helperEnd
				//HelperEnd is the exact opposite (mirrored) version of our helperBegin object 
				//and help us to calculate debug vectors and lines for a perfect shoot.
				//Please refer to the basic geometry references of your choice to understand the math.
				Vector3 dxy = helperBegin.transform.position - shootCircle.transform.position;
				float diff = dxy.magnitude;
				helperEnd.transform.position = shootCircle.transform.position + ((dxy / diff) * currentDistance * -1);
				
				helperEnd.transform.position = new Vector3( helperEnd.transform.position.x,
					                                        helperEnd.transform.position.y,
					                                        -0.5f);				
				
				//debug line from initial position to our current touch position
				Debug.DrawLine(shootCircle.transform.position, helperBegin.transform.position, Color.red);
				//debug line from initial position to maximum power position (mirrored)
				Debug.DrawLine(shootCircle.transform.position, arrowPlane.transform.position, Color.blue);
				//debug line from initial position to the exact opposite position (mirrored) of our current touch position
				Debug.DrawLine(shootCircle.transform.position, (2 * transform.position) - helperBegin.transform.position, Color.yellow);
				//cast ray forward and collect informations
				castRay();
				
				//final vector used to shoot the unit.
				shootDirectionVector = Vector3.Normalize(helperBegin.transform.position - shootCircle.transform.position);
                shootDirectionVector = new Vector2(shootDirectionVector.x, shootDirectionVector.y);

                /* Limit element movement within the shooting circle */
                float zOffset = transform.position.z;
                if ((helperBegin.transform.position - shootCircle.transform.position).magnitude <= shootCircleScale)
                {
                    if (lockElementToFlick)
                    {
                        /* TODO: Check whether helperBegin collides with the element.
                         * And calculate the position on the direction vector to not trigger.
                         */
                        transform.position = elementLockPosition;
                    }
                    else
                    {
                        transform.position = new Vector3(helperBegin.transform.position.x, helperBegin.transform.position.y, zOffset);
                    }
                }
                else
                {
                    Vector3 pos = shootCircle.transform.position + shootDirectionVector * shootCircleScale;
                    transform.position = new Vector3(pos.x, pos.y, zOffset);
                    ManageFlicking();
                }

            // Add rotation randomness to the direction vector depending on the amount of power.
            //arrowJitterRatio = (safeDistance / GlobalGameManager.maxDistance);
            //arrowJitterAngle = arrowJitterRatio * arrowJitterRatio * Random.Range(-10.0f, 10.0f);
            //shootDirectionVector = Quaternion.AngleAxis(arrowJitterAngle, Vector3.forward) * shootDirectionVector;
        }
	}

	//***************************************************************************//
	// Cast the rigidbody's shape forward to see if it is about to hit anything.
	//***************************************************************************//
	void sweepTest (){
		RaycastHit hit;
		if ( GetComponent<Rigidbody>().SweepTest( (helperEnd.transform.position - shootCircle.transform.position).normalized, out hit, 15 )) {
			print("if hit ??? : " + hit.distance + " - " + hit.transform.gameObject.name);
		}
	}

	//***************************************************************************//
	// Cast a ray forward and collect informations like if it hits anything...
	//***************************************************************************//
	private RaycastHit hitInfo;
	private Ray ray;
	void castRay (){
		
		//cast the ray from units position with a normalized direction out of it which is mirrored to our current drag vector.
		ray = new Ray(shootCircle.transform.position, (helperEnd.transform.position - shootCircle.transform.position).normalized );
			
		if(Physics.Raycast(ray, out hitInfo, currentDistance)) {
			GameObject objectHit = hitInfo.transform.gameObject;
			
			//debug line whenever the ray hits something.
			Debug.DrawLine(ray.origin, hitInfo.point, Color.cyan);
					
			//draw reflected vector like a billiard game. this is the out vector which reflects from targets geometry.
			Vector3 reflectedVector = Vector3.Reflect( (hitInfo.point - ray.origin),  hitInfo.normal );
			Debug.DrawRay(hitInfo.point, reflectedVector , Color.gray, 0.2f);
			
			//draw inverted reflected vector (useful for fine-tuning the final shoot)
			Debug.DrawRay(hitInfo.transform.position, reflectedVector * -1, Color.white, 0.2f);
			
			//draw the inverted normal which is more likely to be similar to real world response.
			Debug.DrawRay(hitInfo.transform.position, hitInfo.normal * -3, Color.red, 0.2f);
		}
	}

	//***************************************************************************//
	// Unhide and process the transform and scale of the power Arrow object
	//***************************************************************************//
	void manageArrowTransform (){

		//power arrow codes
		//hide arrowPlane
		arrowPlane.GetComponent<Renderer>().enabled = true;
		shootCircle.GetComponent<Renderer>().enabled = true;
		aimArrowBody.GetComponent<Renderer>().enabled = true;

        //calculate rotation
        Vector3 dir = helperEnd.transform.position - shootCircle.transform.position;
		float outRotation; // between 0 - 360
		
		if(Vector3.Angle(dir, transform.forward) > 90) 
			outRotation = Vector3.Angle(dir, transform.right);
		else
			outRotation = Vector3.Angle(dir, transform.right) * -1;
			
		arrowPlane.transform.eulerAngles = new Vector3(0, 0, outRotation);
		
		//calculate scale
		float scaleCoefX = Mathf.Log(1 + safeDistance/2, 2) * 2.5f * pwr/maxpwr;
		float scaleCoefY = Mathf.Log(1 + safeDistance/2, 2) * 2.5f * pwr/maxpwr; 

		arrowPlane.transform.localScale = new Vector3(1 + scaleCoefX * 3, 1 + scaleCoefY * 3, 0.001f); //default scale

		aimArrow.transform.position = aimPivot.transform.position + new Vector3(0, 0, 0);
        aimArrow.transform.eulerAngles = new Vector3(0, 0, outRotation);
        //aimArrow.transform.eulerAngles = new Vector3(0, 0, outRotation + arrowJitterAngle);

        if (GlobalGameManager.playersTurn && gameObject.tag == "Player") {
			aimArrow.transform.localScale = new Vector3(
				1.5f + (TeamsManager.getTeamSettings(PlayerPrefs.GetInt("PlayerFlag")).z / 2.5f), 
				1.5f + (TeamsManager.getTeamSettings(PlayerPrefs.GetInt ("PlayerFlag")).z / 2.5f), 
				0.01f) * -1;
		} else if (GlobalGameManager.opponentsTurn && gameObject.tag == "Player_2") {
			aimArrow.transform.localScale = new Vector3(
				1.5f + (TeamsManager.getTeamSettings(PlayerPrefs.GetInt ("Player2Flag")).z / 2.5f), 
				1.5f + (TeamsManager.getTeamSettings(PlayerPrefs.GetInt ("Player2Flag")).z / 2.5f), 
				0.01f) * -1;
		}
	}

    //***************************************************************************//
    // Elements collision
    //***************************************************************************//
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Player_2") ||
            other.CompareTag("Player1_Lord") || other.CompareTag("Player2_Lord"))
        {
            // Memorise current dragged element position
            elementLockPosition = transform.position;

            // Distance to the centre of shooting circle is locked
            lockElementToFlick = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Player_2") ||
            other.CompareTag("Player1_Lord") || other.CompareTag("Player2_Lord"))
        {
            // Distance to the centre of shooting circle is locked
            lockElementToFlick = false;
        }
    }

    //***************************************************************************//
    // Element selection
    //***************************************************************************//
    private void OnMouseDown()
    {
        // Initialise helper arrow and limit motion circle
        arrowPlane.transform.position = transform.position + new Vector3(0, 0, 0.03f);
        shootCircle.transform.position = transform.position + new Vector3(0, 0, 0.05f);
        shootCircle.transform.localScale = new Vector3(1 + shootCircleScale * 3, 1 + shootCircleScale * 3, 0.001f); //default scale

        // The mouse helper takes the element's scale
        helperBegin.transform.localScale = transform.localScale;

        // Activate the capsule collider trigger for element proximity detection
        //gameObject.GetComponent<CapsuleCollider>().enabled = true;
        SetProximityDetection(true);

        // Change the colour of the selected element
        selectedElementCircleObject = transform.Find("selectionCircle").gameObject;
        selectedElementRenderer = selectedElementCircleObject.GetComponent<MeshRenderer>();
    }

    //***************************************************************************//
    // Actual shoot function
    //***************************************************************************//
    private void ManageFlicking()
    {
        //Special checks for 2-player game
        if ((GlobalGameManager.playersTurn && gameObject.tag == "Player_2") || (GlobalGameManager.opponentsTurn && gameObject.tag == "Player"))
        {
            arrowPlane.GetComponent<Renderer>().enabled = false;
            shootCircle.GetComponent<Renderer>().enabled = false;
            aimArrowBody.GetComponent<Renderer>().enabled = false;
            return;
        }

        //give the player a second chance to choose another ball if drag on the unit is too low
        print("currentDistance: " + currentDistance);
        if (currentDistance < 0.75f)
        {
            arrowPlane.GetComponent<Renderer>().enabled = false;
            shootCircle.GetComponent<Renderer>().enabled = false;
            aimArrowBody.GetComponent<Renderer>().enabled = false;
            return;
        }

        //But if player wants to shoot anyway:
        //prevent double shooting in a round
        if (!canShoot)
            return;

        //no more shooting is possible	
        canShoot = false;

        // Deactivate the capsule collider to avoid calling the out of bounds
        // routine twice in a row.
        SetProximityDetection(false);

        //keep track of elapsed time after letting the ball go, 
        //so we can findout if ball has stopped and the round should be changed
        //this is the time which user released the button and shooted the ball
        shootTime = Time.time;

        //hide helper arrow object
        arrowPlane.GetComponent<Renderer>().enabled = false;
        shootCircle.GetComponent<Renderer>().enabled = false;
        aimArrowBody.GetComponent<Renderer>().enabled = false;

        //do the physics calculations and shoot the ball
        Vector3 outPower = shootDirectionVector * pwr;

        if (GlobalGameManager.playersTurn && (gameObject.tag == "Player" ||
                                              gameObject.tag == "Player1_Lord") )
        {
            outPower *= (1 + (TeamsManager.getTeamSettings(PlayerPrefs.GetInt("PlayerFlag")).x / 35.0f));

        }
        else if (GlobalGameManager.opponentsTurn && (gameObject.tag == "Player_2" ||
                                                     gameObject.tag == "Player2_Lord") )
        {
            outPower *= (1 + (TeamsManager.getTeamSettings(PlayerPrefs.GetInt("Player2Flag")).x / 35.0f));
        }

        //always make the player to move only in x-y plane and not on the z direction
        print("shoot power: " + outPower.magnitude);
        GetComponent<Rigidbody>().AddForce(outPower, ForceMode.Impulse);

        //Update game summary
        //Please notice that both player1 and player2 (human players) are using this class to shoot their units.
        //so we need to distinguish each one when we want to monitor their stats
        if (GlobalGameManager.gameMode == 0)
        {

            //for player vs AI mode
            if (outPower.magnitude > 24)
                GlobalGameManager.playerShoots++;
            else
                GlobalGameManager.playerPasses++;

        }
        else if (GlobalGameManager.gameMode == 1)
        {

            //for player1 vs player2 mode
            if (outPower.magnitude > 24)
            {
                if (gameObject.tag == "Player")
                    GlobalGameManager.playerShoots++;
                else if (gameObject.tag == "Player_2")
                    GlobalGameManager.opponentShoots++;
            }
            else
            {
                if (gameObject.tag == "Player")
                    GlobalGameManager.playerPasses++;
                else if (gameObject.tag == "Player_2")
                    GlobalGameManager.opponentPasses++;
            }

        }
        //End - Update game summary

        //change the turn
        if (GlobalGameManager.gameMode == 0)
            StartCoroutine(gameController.GetComponent<GlobalGameManager>().managePostShoot("Player"));
        else if (GlobalGameManager.gameMode == 1 || GlobalGameManager.gameMode == 2)
            StartCoroutine(gameController.GetComponent<GlobalGameManager>().managePostShoot(gameObject.tag));
    }

    void OnMouseUp (){
        // Restore original flicking settings
        selectedElementRenderer.material.color = Color.white;
        dragTime = 0;
        selectedElementCircleObject.GetComponent<ScaleAnimator>().UpdateAnimationScale(initSelectCircleScale);

        // Manage the flicking of the selected element
        ManageFlicking();
    }

}
