using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class PlayerAIV2 : MonoBehaviour {
		
	///*************************************************************************///
	/// Main Player AI class.
	/// why do we need a player AI class?
	/// This class has a reference to all player-1 (and player-2) units, their formation and their position
	/// and will be used to setup new formations for these units at the start of the game or when a goal happens.
	///*************************************************************************///

	public Texture2D[] availableFlags;          //array of all available teams

    public static GameObject[] playerTeam;		//array of all player-1 units
    public static int playerFormation;			//player-1 formation
    public static int playerFlag;				//player-1 team flag

    //for two player game
    public static GameObject[] player2Team;		//array of all player-2 units
    public static int player2Formation;			//player-2 formation
    public static int player2Flag;				//player-2 team flag

    //flags
    internal bool canChangeFormation;

    //team selection flag
    enum teamSelected
    {
        TEAM_PLAYER_1,
        TEAM_PLAYER_2
    };

    //*****************************************************************************
    // Initialise players units, settings and formation.
    //*****************************************************************************
    private void SetupPlayerUnits(teamSelected team)
    {
        string playerFormationTagStr = "";
        string playerFlagTagStr = "";
        string playerUnitTagStr = "";
        string playerLordTagStr = "";
        string playerUnitNameStr = "";
        ref int formationId = ref playerFormation;
        ref int flagId = ref playerFlag;
        ref GameObject[] teamObj = ref playerTeam;

        switch (team)
        {
            case teamSelected.TEAM_PLAYER_1:
                playerFormationTagStr = "PlayerFormation";
                playerFlagTagStr = "PlayerFlag";
                playerUnitTagStr = "Player";
                playerLordTagStr = "Player1_Lord";
                playerUnitNameStr = "PlayerUnit-";
                formationId = ref playerFormation;
                flagId = ref playerFlag;
                teamObj = ref playerTeam;
                break;
            case teamSelected.TEAM_PLAYER_2:
                playerFormationTagStr = "Player2Formation";
                playerFlagTagStr = "Player2Flag";
                playerUnitTagStr = "Player_2";
                playerLordTagStr = "Player2_Lord";
                playerUnitNameStr = "Player2Unit-";
                formationId = ref player2Formation;
                flagId = ref player2Flag;
                teamObj = ref player2Team;
                break;
            default:
                Debug.LogError("The selected team does not exist/is invalid.");
                break;
        }

        //fetch player's formation
        if (PlayerPrefs.HasKey(playerFormationTagStr))
            formationId = PlayerPrefs.GetInt(playerFormationTagStr);
        else
            formationId = 0; //Default Formation

        //fetch player's flag
        if (PlayerPrefs.HasKey(playerFlagTagStr))
            flagId = PlayerPrefs.GetInt(playerFlagTagStr);
        else
            flagId = 0; //Default team

        // cache all units
        GameObject[] units = GameObject.FindGameObjectsWithTag(playerUnitTagStr);
        GameObject lord = GameObject.FindWithTag(playerLordTagStr);
        if (units.Length == 0 || lord == null)
        {
            Debug.LogError("No elements with the given tag found.");
            return;
        }

        // Allocate memory to store the lord, attacker and defender units.
        teamObj = new GameObject[units.Length + 1];

        // cache player's lord
        teamObj[0] = GameObject.FindWithTag(playerLordTagStr);

        // cache player's units
        for (int k = 0; k < units.Length; k++)
        {
            teamObj[k + 1] = units[k];
        }

        // Units renaming and texture allocation.
        int i = 1;
        foreach (GameObject unit in teamObj)
        {
            //Optional
            unit.name = playerUnitNameStr + i;
            unit.GetComponent<playerControllerV2>().unitIndex = i;
            unit.GetComponent<Renderer>().material.mainTexture = availableFlags[flagId];
            i++;
        }
    }

    //*****************************************************************************
    // Init. 
    //*****************************************************************************
    void init() {

        // Resolve object aliases
        playerTeam = GlobalGameManager.playerTeam;
        player2Team = GlobalGameManager.player2Team;
        playerFormation = GlobalGameManager.playerFormation;
        player2Formation = GlobalGameManager.player2Formation;
        playerFlag = GlobalGameManager.playerFlag;
        player2Flag = GlobalGameManager.player2Flag;

        canChangeFormation = true;  //we just change the formation at the start of the game. No more change of formation afterwards!

        SetupPlayerUnits(teamSelected.TEAM_PLAYER_1);
        SetupPlayerUnits(teamSelected.TEAM_PLAYER_2);
    }


	void Start (){

		//run the starting commands
		init ();

		StartCoroutine(changeFormation(playerTeam, playerFormation, 1, 1));

		//For two-player mode,
		//if(GlobalGameManager.gameMode == 1) {
				StartCoroutine(changeFormation(player2Team, player2Formation, 1, -1));
		//}
			
		canChangeFormation = false;
	}

	//*****************************************************************************
	// changeFormation function take all units, selected formation and side of the player (left half or right half)
	// and then position each unit on it's destination.
	// speed is used to fasten the translation of units to their destinations.
	//*****************************************************************************
	public IEnumerator changeFormation ( GameObject[] _team ,   int _formationIndex ,   float _speed ,   int _dir  ){

		//cache the initial position of all units
		List<Vector3> unitsSartingPosition = new List<Vector3>();
		foreach(GameObject unit in _team) {
			unitsSartingPosition.Add(unit.transform.position); //get the initial postion of this unit for later use.
			unit.GetComponent<MeshCollider>().enabled = false;	//no collision for this unit till we are done with re positioning.
		}
		
		float t = 0;
		while(t < 1) {
			t += Time.deltaTime * _speed;
			for(int cnt = 0; cnt < _team.Length; cnt++) {
                _team[cnt].transform.position = new Vector3(Mathf.SmoothStep(unitsSartingPosition[cnt].x,
                                                   FormationManager.getPositionInFormation(_formationIndex, cnt).x * _dir,
                                                   t),
                                                   Mathf.SmoothStep(unitsSartingPosition[cnt].y,
                                                   FormationManager.getPositionInFormation(_formationIndex, cnt).y,
                                                   t),
                                                   FormationManager.getPositionInFormation(_formationIndex, cnt).z);
            }		
			yield return 0;
		}
		
		if(t >= 1) {
			canChangeFormation = true;
			foreach(GameObject unit in _team)
				unit.GetComponent<MeshCollider>().enabled = true; //collision is now enabled.
		}
	}


	//*****************************************************************************
	// move the unit to its position
	//*****************************************************************************
	public IEnumerator goToPosition ( GameObject[] unit, Vector3 pos,   float _speed  ){

		//first we need to completely freeze the unit
		unit[0].GetComponent<Rigidbody>().velocity = Vector3.zero;
		unit[0].GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
		print (unit[0].name + " position is fixed.");

		Vector3 unitsSartingPosition = unit[0].transform.position;
		unit[0].GetComponent<MeshCollider>().enabled = false;	//no collision for this unit till we are done with re positioning.

		float t = 0;
		while(t < 1) {
			t += Time.deltaTime * _speed;
			unit[0].transform.position = new Vector3( Mathf.SmoothStep(unitsSartingPosition.x, pos.x, t),
			                                   		  Mathf.SmoothStep(unitsSartingPosition.y, pos.y, t),
			                                     	  unitsSartingPosition.z );
				
			yield return 0;
		}

		if(t >= 1) {
			unit[0].GetComponent<MeshCollider>().enabled = true;
		}
	}

}
