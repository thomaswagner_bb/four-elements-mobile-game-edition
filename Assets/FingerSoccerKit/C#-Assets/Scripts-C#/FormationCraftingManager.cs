﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormationCraftingManager : MonoBehaviour
{
    public GameObject[] listOfInventoryContainers;
    public GameObject[] listOfElements;
    //private GameObject selectedInventoryContainer;
    public GameObject playerInventory;
    //private GameObject element;

    // Start is called before the first frame update
    void Start()
    {
        // Hide inventory and elements
        foreach (GameObject obj in listOfInventoryContainers)
            obj.SetActive(false);

        foreach (GameObject obj in listOfElements)
            obj.SetActive(false);


        //element = listOfElements[0];
        //element.transform.position = new Vector3(0, 0, 0);
        //element.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //DisplayInventory(Input.mousePosition);
            Debug.Log(Input.mousePosition);
        }
    }

    private void DisplayInventory(Vector3 position)
    {
        playerInventory.transform.position = position;

        // Hide inventory and elements
        foreach (GameObject obj in listOfInventoryContainers)
            obj.SetActive(true);

        foreach (GameObject obj in listOfElements)
            obj.SetActive(true);
    }
}
