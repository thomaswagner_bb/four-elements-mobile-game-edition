﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactersAction : MonoBehaviour
{
    public GameObject FriendCharacter;
    public GameObject EvilCharacter;
    const float FixedZ = -1f;

    // Start is called before the first frame update
    void Start()
    {
        // Place characters outside of the screen
        FriendCharacter.transform.position = new Vector3(28f, 0);
        EvilCharacter.transform.position = new Vector3(-28f, 0);

        StartCoroutine(SlideCharacterInsideScreen(FriendCharacter, 1.0f, FriendCharacter.transform.position,
                                                          new Vector3(10f, 0)));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator SlideCharacterInsideScreen(GameObject obj, float speed, Vector3 from, Vector3 to)
    {
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime * speed;
            obj.transform.position = new Vector3(Mathf.SmoothStep(from.x, to.x, t),
                                                 Mathf.SmoothStep(from.y, to.y, t),
                                                 FixedZ);

            yield return 0;
        }
    }
}
